package org.tdr;

import org.tdr.dto.AttendessDTO;
import org.tdr.dto.CountryDTO;
import org.tdr.dto.OrganisationDTO;
import org.tdr.dto.RegistrationAttendessDTO;
import org.tdr.model.Attendees;
import org.tdr.model.Country;
import org.tdr.model.Organisation;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Converter {

    public static AttendessDTO convertAttendeesToAttendeesDTO(Attendees attendees) {
        return AttendessDTO.builder()
                .id(attendees.getId())
                .username(attendees.getUsername())
                .mail(attendees.getMail())
                .firstName(attendees.getFirstName())
                .secondName(attendees.getSecondName())
                .phone(attendees.getPhone())
                .plz(attendees.getPhone())
                .organisation(attendees.getOrganisation().getId())
                .country(attendees.getCountry().getId())
                .build();
    }

    public static OrganisationDTO convertOrganistationToOrganistationDTO(Organisation organisation) {
        return OrganisationDTO.builder().id(organisation.getId()).name(organisation.getName()).shortName(organisation.getShortName()).build();
    }

    public static List<OrganisationDTO> convertOrganistationsToOrganistationDTOs(Collection<Organisation> organisations) {
        return organisations.stream().map(Converter::convertOrganistationToOrganistationDTO).collect(Collectors.toList());
    }

    public static CountryDTO convertCountyToCountyDTO(Country country) {
        return CountryDTO.builder().id(country.getId()).name(country.getName()).shortName(country.getShortName()).build();
    }

    public static List<CountryDTO> convertCountiesToCountyDTOs(Collection<Country> countries) {
        return countries.stream().map(Converter::convertCountyToCountyDTO).collect(Collectors.toList());
    }


}

