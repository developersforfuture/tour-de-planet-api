package org.tdr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tdr.Converter;
import org.tdr.dto.AttendessDTO;
import org.tdr.dto.RegistrationAttendessDTO;
import org.tdr.model.Attendees;
import org.tdr.repository.AttendeesRespository;
import org.tdr.repository.CountryRespository;
import org.tdr.repository.OrganisationRespository;
import org.tdr.security.WebSecurityConfig;

@Service
public class AttendeesService {

    @Autowired
    private AttendeesRespository attendeesRespository;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    private OrganisationRespository organisationRespository;

    @Autowired
    private CountryRespository countryRespository;

    public Attendees createAttendees(RegistrationAttendessDTO attendessDTO) {
        Attendees attendees = Attendees.builder()
                .username(attendessDTO.getUsername())
                .password(webSecurityConfig.passwordEncoder().encode(attendessDTO.getPassword()))
                .mail(attendessDTO.getMail())
                .firstName(attendessDTO.getFirstName())
                .secondName(attendessDTO.getSecondName())
                .phone(attendessDTO.getPhone())
                .plz(attendessDTO.getPlz())
                .isCycling(attendessDTO.getIsCycling())
                .isPress(attendessDTO.getIsPress())
                .isOrga(attendessDTO.getIsOrga())
                .organisation(organisationRespository.findById(attendessDTO.getOrganisation()).get())
                .country(countryRespository.findById(attendessDTO.getCountry()).get()).build();
        if(attendees.getOrganisation() == null || attendees.getCountry() == null){
            return null;
        }
        try {
            attendees = attendeesRespository.save(attendees);
        } catch (Exception ex) {
            return null;
        }
        return attendees;
    }

    public Attendees updateAttendees(AttendessDTO attendessDTO) {
        Attendees attendees = attendeesRespository.findById((long) attendessDTO.getId());
        attendees.setPhone(attendessDTO.getPhone());
        attendees.setSecondName(attendessDTO.getSecondName());
        return attendees;
    }

    public Attendees getAttendees(Long id) {
        return attendeesRespository.findById(id).get();
    }
}
