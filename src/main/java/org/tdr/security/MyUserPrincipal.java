package org.tdr.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.tdr.model.Attendees;

import java.util.Collection;

public class MyUserPrincipal implements UserDetails {
    private Attendees attendees;

    public MyUserPrincipal(Attendees attendees) {
        this.attendees = attendees;
    }

    public Attendees getAttendees() {
        return attendees;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return attendees.getPassword();
    }

    @Override
    public String getUsername() {
        return attendees.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
