package org.tdr.repository;

import org.springframework.data.repository.CrudRepository;
import org.tdr.model.Organisation;

import java.util.List;

public interface OrganisationRespository extends CrudRepository<Organisation, Long> {

    Organisation findById(long id);

    Organisation findByName(String username);

    List<Organisation> findAll();
}
