package org.tdr.repository;

import org.springframework.data.repository.CrudRepository;
import org.tdr.model.Attendees;

public interface AttendeesRespository extends CrudRepository<Attendees, Long> {

    Attendees findById(long id);

    Attendees findByUsername(String username);

    Attendees countAllByUsername(String username);
}
