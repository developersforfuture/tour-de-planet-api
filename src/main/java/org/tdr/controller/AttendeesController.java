package org.tdr.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.tdr.Converter;
import org.tdr.dto.AttendessDTO;
import org.tdr.dto.RegistrationAttendessDTO;
import org.tdr.model.Attendees;
import org.tdr.security.*;
import org.tdr.service.AttendeesService;

@RestController
@RequestMapping("/api/attendees")
public class AttendeesController {

    @Autowired
    private AttendeesService attendeesService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;

    @PostMapping
    ResponseEntity<AttendessDTO> saveAttendess(@RequestBody AttendessDTO attendessDTO) {
        //password and username not possible
        if (StringUtils.isEmpty(attendessDTO.getFirstName()) ||
                StringUtils.isEmpty(attendessDTO.getPhone()) ||
                attendessDTO.getId() == null ||
                StringUtils.isEmpty(attendessDTO.getMail())) {
            return ResponseEntity.badRequest().build();
        }
        //TODO validate input
        Attendees attendees = attendeesService.updateAttendees(attendessDTO);
        return ResponseEntity.ok(Converter.convertAttendeesToAttendeesDTO(attendees));
    }

    @PostMapping("/create")
    ResponseEntity<JwtResponse> createAttendess(@RequestBody RegistrationAttendessDTO attendessDTO) {
        if (StringUtils.isEmpty(attendessDTO.getFirstName()) ||
                StringUtils.isEmpty(attendessDTO.getUsername()) ||
                StringUtils.isEmpty(attendessDTO.getPassword()) ||
                StringUtils.isEmpty(attendessDTO.getPhone()) ||
                StringUtils.isEmpty(attendessDTO.getSecondName()) ||
                StringUtils.isEmpty(attendessDTO.getMail())) {
            return ResponseEntity.badRequest().build();
        }
        //TODO validate input
        Attendees attendees = attendeesService.createAttendees(attendessDTO);
        if (attendees == null) {
            return ResponseEntity.badRequest().build();
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(attendessDTO.getUsername(), attendessDTO.getPassword()));
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(attendessDTO.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping
    ResponseEntity<AttendessDTO> getOwnAttendeeseInfo() {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


        AttendessDTO attendessDTO = Converter.convertAttendeesToAttendeesDTO(attendeesService.getAttendees(myUserPrincipal.getAttendees().getId()));
        return ResponseEntity.ok(attendessDTO);
    }
}
