INSERT INTO "organisation" (id,name,short_name) values (2,'Keine Organisations', '');
INSERT INTO "organisation" (id,name,short_name) values (0,'Fridays For Future', 'FFF');
INSERT INTO "organisation" (id,name,short_name) values (1,'Extinction Rebelion', 'XR');

INSERT INTO "country" (id,name,short_name) values (0,'Deutschland', 'DE');
INSERT INTO "country" (id,name,short_name) values (1,'Polen', 'PL');
INSERT INTO "country" (id,name,short_name) values (2,'Frankreich', 'FR');
INSERT INTO "country" (id,name,short_name) values (3,'England', 'GB');
INSERT INTO "country" (id,name,short_name) values (4,'Dänemark', 'DE');
INSERT INTO "country" (id,name,short_name) values (5,'Deutschland', 'DK');

INSERT INTO "attendees" (id, username, password, first_name, second_name, mail, phone,organisation_id,is_cycling,is_press,is_orga,country_id)
values (1000, 'test', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'First Name', 'Second Name',
        'a@b.de', '12345',0,true,false,false,1);